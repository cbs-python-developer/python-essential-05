"""Homework 5 Task 6 implementation."""

import inspect

from hw52_contact import Contact, UpdateContact


for class_obj in (Contact, UpdateContact):
    print(f"\n{class_obj.__name__}")
    for method in inspect.getmembers(Contact, predicate=inspect.isfunction):
        print(method)
