"""Homework 5 Task 3 implementation."""

from hw52_contact import Contact, UpdateContact, Message


def print_hasattr(items: list, cls_obj: type) -> None:
    """Evaluate hasattr and print the result."""
    for item in items:
        command = f"hasattr({cls_obj.__name__}, {item})"
        print(f"{command: <36} {hasattr(cls_obj, item)}")


def print_getattr(items: list, cls_obj: type) -> None:
    """Evaluate getattr and print the result."""
    for item in items:
        command = f"getattr({cls_obj.__name__}, {item})"
        print(f"{command: <36} {getattr(cls_obj, item)}")


if __name__ == '__main__':

    print('\n--------TASK 3')

    for class_obj in (Contact, UpdateContact):
        print(f"\n[{class_obj.__name__}]\n")

        attributes = [key for key in class_obj.__dict__]

        # HAS
        print_hasattr(attributes, class_obj)
        print()

        # GET
        print_getattr(attributes, class_obj)
        print()

        # DEL
        for attr in attributes:
            command_del = f"delattr({class_obj.__name__}, {attr})"
            print(f"{command_del: <36}", end=' ')
            try:
                delattr(class_obj, attr)
                print("SUCCESS")
            except (TypeError, AttributeError) as err:
                print(f"ERROR: {err}")
        print()
        print(f"{class_obj.__name__}.__dict__ >>> {class_obj.__dict__}")
        print()
        print_hasattr(attributes, class_obj)

    # SET
    print("\n[Contact]\n")
    print(f"{Contact.__name__}.__dict__ >>> {Contact.__dict__}")
    print("----Setting attributes ...")
    setattr(Contact, '__module__', 'Contact')
    setattr(Contact, '__doc__', 'New __doc__')
    setattr(Contact, '__init__', lambda x: x + 1)
    setattr(Contact, 'surname', 'Fund')
    setattr(Contact, 'name', 'James')
    setattr(Contact, 'age', 77)
    setattr(Contact, 'mob_phone', 380441234567)
    setattr(Contact, 'email', 'new.email@yahoo.com')
    setattr(Contact, 'get_contact', lambda x: reversed(x))
    setattr(Contact, '__str__', 'New __str__ function')
    setattr(Contact, 'sent_message', 'Updated sent function')
    try:
        setattr(Contact, '__dict__', {1: '1'})
    except AttributeError as err:
        print(f"setattr(Contact, '__dict__', {{1: '1'}}) >>> ERROR: {err}")
    setattr(Contact, '__weakref__', 'New attribute')
    print()
    print_getattr([key for key in Contact.__dict__], Contact)

    print("\n[UpdateContact]\n")
    print(f"{UpdateContact.__name__}.__dict__ >>> {UpdateContact.__dict__}")
    print("----Setting attributes ...")
    setattr(UpdateContact, '__module__', '__new_module_name__')
    setattr(UpdateContact, '__doc__', 'New class description')
    setattr(UpdateContact, '__init__', '__newinit__')
    setattr(UpdateContact, 'job', 'CTO')
    setattr(UpdateContact, '__str__', '__newstr__')
    setattr(UpdateContact, 'get_message', lambda x, y, txt: Message(x, y, txt))
    print()
    print_getattr([key for key in UpdateContact.__dict__], UpdateContact)
