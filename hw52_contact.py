"""Homework 5 Task 2 implementation."""


class Contact:
    """A class to represent a contact information entry."""

    def __init__(
            self,
            surname: str,
            name: str,
            age: int,
            mob_phone: int,
            email: str,
    ) -> None:
        self.surname = surname
        self.name = name
        self.age = age
        self.mob_phone = mob_phone
        self.email = email

    @property
    def surname(self) -> str:
        """The last name of a contact."""
        return self._surname

    @surname.setter
    def surname(self, value: str) -> None:
        self._surname = value

    @property
    def name(self) -> str:
        """The first name of a contact."""
        return self._name

    @name.setter
    def name(self, value: str) -> None:
        self._name = value

    @property
    def age(self) -> int:
        """Age of a contact."""
        return self._age

    @age.setter
    def age(self, value: int) -> None:
        self._age = value

    @property
    def mob_phone(self) -> int:
        """Mobile phone number of a contact."""
        return self._mob_phone

    @mob_phone.setter
    def mob_phone(self, value: int) -> None:
        self._mob_phone = value

    @property
    def email(self) -> str:
        """Email address of a contact."""
        return self._email

    @email.setter
    def email(self, value: str) -> None:
        self._email = value

    def get_contact(self) -> dict:
        """Return all contact attributes."""
        return {
            'surname': self.surname,
            'name': self.name,
            'age': self.age,
            'mob_phone': self.mob_phone,
            'email': self.email,
        }

    def __str__(self) -> str:
        return (f"{self.name} {self.surname}, {self.age} | "
                f"mobile: {self.mob_phone} | "
                f"email: {self.email}")

    def sent_message(self, receiver: 'Contact', content: str) -> None:
        """Send a message to another contact."""
        message = Message(self, receiver, content)
        message.sent()


class UpdateContact(Contact):
    """A class to represent an enhanced contact."""

    def __init__(
            self,
            surname: str,
            name: str,
            age: int,
            mob_phone: int,
            email: str,
            job: str
    ) -> None:
        super().__init__(surname, name, age, mob_phone, email)
        self.job = job

    @property
    def job(self) -> str:
        return self._job

    @job.setter
    def job(self, value: str) -> None:
        self._job = value

    def __str__(self) -> str:
        return super().__str__() + f" | job: {self.job}"

    def get_message(self, message: 'Message') -> tuple[Contact, str]:
        """Receive a message from another contact."""
        if message.receiver == self:
            return message.receive()


class Message:
    """A class to emulate message behaviour."""

    def __init__(
            self, sender: Contact, receiver: Contact, content: str
    ) -> None:
        self.sender = sender
        self.receiver = receiver
        self.content = content

    def sent(self) -> None:
        """Sent the message to the receiver."""
        pass

    def receive(self) -> tuple[Contact, str]:
        """Accept the message."""
        return self.sender, self.content


if __name__ == '__main__':

    print('\n--------TASK 2')

    con1 = Contact(
        'Doe', 'John', 36, 120023984576, 'johnny88@gmail.com'
    )
    ucon2 = UpdateContact(
        'Diamond', 'Matt', 42, 210192837465, 'diamatt@ukr.net', 'driver'
    )
    for contact in (con1, ucon2):
        class_obj = contact.__class__
        class_name = class_obj.__name__
        print(f"\nInstance of {class_name} >>> {contact}")
        print(f"Instance.__dict__: {contact.__dict__}")
        print(f"{class_name}.__dict__ >>> {class_obj.__dict__}")
        print(f"{class_name}.__base__ >>> {class_obj.__base__}")
        print(f"{class_name}.__bases__ >>> {class_obj.__bases__}")
