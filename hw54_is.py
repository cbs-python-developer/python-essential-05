"""Homework 5 Task 4 implementation."""

from hw52_contact import Contact, UpdateContact


print('\n--------TASK 4')

con1 = Contact(
    'Doe', 'John', 36, 120023984576, 'johnny88@gmail.com'
)
ucon2 = UpdateContact(
    'Diamond', 'Matt', 42, 210192837465, 'diamatt@ukr.net', 'driver'
)

for instance_obj in (con1, ucon2):
    class_name = instance_obj.__class__.__name__
    print()
    for class_obj in (Contact, UpdateContact, object):
        print(
            f"isinstance(<Instance of {class_name}>, {class_obj.__name__}) >>> "
            f"{isinstance(instance_obj, class_obj)}"
        )

for class_obj in (Contact, UpdateContact):
    class_name = class_obj.__name__
    print()
    for classinfo_obj in (Contact, UpdateContact, object):
        classinfo_name = classinfo_obj.__name__
        print(
            f"issubclass({class_name}, {classinfo_name}) >>> "
            f"{issubclass(class_obj, classinfo_obj)}"
        )
