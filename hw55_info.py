"""Homework 5 Task 5 implementation."""

import inspect
from typing import Any

from hw52_contact import Contact, UpdateContact


def print_info(*items: Any) -> None:
    """Print all the members of an object."""
    for item in items:
        if inspect.isclass(item):
            print(f"\n{item.__name__}")
        else:
            print(f"\n{item}")
        for element in inspect.getmembers(item):
            print(element)


if __name__ == '__main__':

    print('\n--------TASK 5')

    con1 = Contact(
        'Doe', 'John', 36, 120023984576, 'johnny88@gmail.com'
    )
    ucon2 = UpdateContact(
        'Diamond', 'Matt', 42, 210192837465, 'diamatt@ukr.net', 'driver'
    )

    print_info(Contact, con1, UpdateContact, ucon2)

    print("\n----Delete attribute 'job'")
    delattr(UpdateContact, 'job')

    print_info(Contact, con1, UpdateContact, ucon2)
